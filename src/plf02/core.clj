(ns plf02.core)

(defn función-associative?-1
  [xs]
  (associative? xs))

(defn función-associative?-2
  [ys]
  (associative? ys))

(defn función-associative?-3
  [zs]
  (associative? zs))

(función-associative?-1 [1 2 4 5])
(función-associative?-2 {:a 1 :b 2})
(función-associative?-3 #{1 4 5 6})

(defn función-boolean?-1
  [x]
  (boolean? x))

(defn función-boolean?-2
  [x]
  (boolean? x))

(defn función-boolean?-3
  [x]
  (boolean? x))

(función-boolean?-1 false)
(función-boolean?-2 (new Boolean "false"))
(función-boolean?-3 nil)

(defn función-char?-1
  [x]
  (char? x))

(defn función-char?-2
  [x]
  (char? x))

(defn función-char?-3
  [x]
  (char? x))

(función-char?-1 "a")
(función-char?-2 40)
(función-char?-3 (first "clojure"))

(defn función-coll?-1
  [x]
  (coll? x))

(defn función-coll?-2
  [x]
  (coll? x))

(defn función-coll?-3
  [x]
  (coll? x))

(función-coll?-1 [1 2 3])
(función-coll?-2 #{:a 1 :b 2 :c 3})
(función-coll?-3 '("clojure"))

(defn función-decimal?-1
  [n]
  (decimal? n))

(defn función-decimal?-2
  [n]
  (decimal? n))

(defn función-decimal?-3
  [n]
  (decimal? n))

(función-decimal?-1 1.0)
(función-decimal?-2 9999)
(función-decimal?-3 1M)

(defn función-double?-1
  [x]
  (double? x))

(defn función-double?-2
  [x]
  (double? x))

(defn función-double?-3
  [x]
  (double? x))

(función-double?-1 1.0)
(función-double?-2 (new Double "1"))
(función-double?-3 1.0M)

(defn función-float?-1
  [x]
  (float? x))

(defn función-float?-2
  [x]
  (float? x))

(defn función-float?-3
  [x]
  (float? x))

(función-float?-1 0)
(función-float?-2 0.5)
(función-float?-3 0.1M)

(defn función-ident?-1
  [x]
  (ident? x))

(defn función-ident?-2
  [x]
  (ident? x))

(defn función-ident?-3
  [x]
  (ident? x))

(función-ident?-1 :clojure)
(función-ident?-2 'abc)
(función-ident?-3 "hola")

(defn función-indexed?-1
  [xs]
  (indexed? xs))

(defn función-indexed?-2
  [ys]
  (indexed? ys))

(defn función-indexed?-3
  [zs]
  (indexed? zs))

(función-indexed?-1 [5 4])
(función-indexed?-1 {\a \b \c \d})
(función-indexed?-1 #{:a 1 :b 2 :c 3})

(defn función-int?-1
  [x]
  (int? x))

(defn función-int?-2
  [x]
  (int? x))

(defn función-int?-3
  [x]
  (int? x))

(función-int?-1 56)
(función-int?-2 92.0)
(función-int?-3 "1")

(defn función-integer?-1
  [n]
  (integer? n))

(defn función-integer?-2
  [n]
  (integer? n))

(defn función-integer?-3
  [n]
  (integer? n))

(función-integer?-1 1)
(función-integer?-2 1.0)
(función-integer?-3 -1)

(defn función-keyword?-1
  [x]
  (keyword? x))

(defn función-keyword?-2
  [x]
  (keyword? x))

(defn función-keyword?-3
  [x]
  (keyword? x))

(función-keyword?-1 :x)
(función-keyword?-2 true)
(función-keyword?-3 'x)

(defn función-list?-1
  [x]
  (list? x))

(defn función-list?-2
  [x]
  (list? x))

(defn función-list?-3
  [x]
  (list? x))

(función-list?-1 '( 1 2 3))
(función-list?-1 '([4 5 6]))
(función-list?-1 #{\a \b \c})

(defn función-map-entry?-1
  [x]
  (map-entry? x))

(defn función-map-entry?-2
  [x]
  (map-entry? x))

(defn función-map-entry?-3
  [x]
  (map-entry? x))

(función-map-entry?-1 '(5 4 7 8))
(función-map-entry?-2 (first {:a 1 :b 2 :c 3}))
(función-map-entry?-3 {:a 1 :b 2 :c 3})

(defn función-map?-1
  [x]
  (map? x))

(defn función-map?-2
  [x]
  (map? x))

(defn función-map?-3
  [x]
  (map? x))

(función-map?-1 {:A 1 :B 2 :C 3 :D 4})
(función-map?-2 (hash-map :a 1 :b 2))
(función-map?-3 #{:a :b :c})

(defn función-nat-int?-1
  [x]
  (nat-int? x))

(defn función-nat-int?-2
  [x]
  (nat-int? x))

(defn función-nat-int?-3
  [x]
  (nat-int? x))

(función-nat-int?-1 0)
(función-nat-int?-2 -1)
(función-nat-int?-3 1/2)

(defn función-number?-1
  [x]
  (number? x))

(defn función-number?-2
  [x]
  (number? x))

(defn función-number?-3
  [x]
  (number? x))

(función-number?-1 12)
(función-number?-2 "25")
(función-number?-3 [5])

(defn función-pos-int?-1
  [x]
  (pos-int? x))

(defn función-pos-int?-2
  [x]
  (pos-int? x))

(defn función-pos-int?-3
  [x]
  (pos-int? x))

(función-pos-int?-1 1454)
(función-pos-int?-2 -1554)
(función-pos-int?-3 15.54)

(defn función-ratio?-1
  [n]
  (ratio? n))

(defn función-ratio?-2
  [n]
  (ratio? n))

(defn función-ratio?-3
  [n]
  (ratio? n))

(función-ratio?-1 244/8945)
(función-ratio?-2 546)
(función-ratio?-3 264.246)

(defn función-rational?-1
  [n]
  (rational? n))

(defn función-rational?-2
  [n]
  (rational? n))

(defn función-rational?-3
  [n]
  (rational? n))

(función-rational?-1 54)
(función-rational?-1 215.0)
(función-rational?-1 587/535)

(defn función-seq?-1
  [x]
  (seq? x))

(defn función-seq?-2
  [x]
  (seq? x))

(defn función-seq?-3
  [x]
  (seq? x))

(función-seq?-1 11)
(función-seq?-2 [10])
(función-seq?-3 (seq [9]))

(defn función-seqable?-1
  [x]
  (seqable? x))

(defn función-seqable?-2
  [x]
  (seqable? x))

(defn función-seqable?-3
  [x]
  (seqable? x))

(función-seqable?-1 [5 69])
(función-seqable?-2 "clojure")
(función-seqable?-3 nil)

(defn función-sequential?-1
  [x]
  (sequential? x))

(defn función-sequential?-2
  [x]
  (sequential? x))

(defn función-sequential?-3
  [x]
  (sequential? x))

(función-sequential?-1 [1 2 3 4])
(función-sequential?-2 (range 10 50))
(función-sequential?-3 {:a 1 :b 2 :c 3})

(defn función-set?-1
  [x]
  (set? x))

(defn función-set?-2
  [x]
  (set? x))

(defn función-set?-3
  [x]
  (set? x))

(función-set?-1 #{3 4 5 6})
(función-set?-2 {:a 1 :b 2 :c 3})
(función-set?-3 [1 4 6 8])

(defn función-some?-1
  [x]
  (some? x))

(defn función-some?-2
  [x]
  (some? x))

(defn función-some?-3
  [x]
  (some? x))

(función-some?-1 [1 2 5 4])
(función-some?-2 #{:a 1 :b 2})
(función-some?-3 nil)

(defn función-string?-1
  [x]
  (string? x))

(defn función-string?-2
  [x]
  (string? x))

(defn función-string?-3
  [x]
  (string? x))

(función-string?-1 "clojure")
(función-string?-2 ["hi" "hola" "ana"])
(función-string?-3 \A)

(defn función-symbol?-1
  [x]
  (symbol? x))

(defn función-symbol?-2
  [x]
  (symbol? x))

(defn función-symbol?-3
  [x]
  (symbol? x))

(función-symbol?-1 'a)
(función-symbol?-2 1)
(función-symbol?-3 :a)

(defn función-vector?-1
  [x]
  (vector? x))

(defn función-vector?-2
  [x]
  (vector? x))

(defn función-vector?-3
  [x]
  (vector? x))

(función-vector?-1 [10 20 30])
(función-vector?-2 '(10 20 30))
(función-vector?-3 [#{:a 1 :b 2 :c 3 :d 4}])

(defn función-drop-1
  [n xs]
  (drop n xs))

(defn función-drop-2
  [n ys]
  (drop n ys))

(defn función-drop-3
  [n zs]
  (drop n zs))

(función-drop-1 3 [1 2 3 4 5])
(función-drop-2 2 [1 2 3 4 5 6])
(función-drop-3 -2 [-3 -2 1 0 1 2 3 4])

(defn función-drop-last-1
  [n xs]
  (drop-last n xs))

(defn función-drop-last-2
  [n ys]
  (drop-last n ys))

(defn función-drop-last-3
  [n zs]
  (drop-last n zs))

(función-drop-last-1 2 [1 2 3 4])
(función-drop-last-2 3 {:a 1 :b 2 :c 3 :d 4})
(función-drop-last-3 5 [1 2 3 4])

(defn función-drop-while-1
  [pred xs]
  (drop-while pred xs))

(defn función-drop-while-2
  [pred ys]
  (drop-while pred ys))

(defn función-drop-while-3
  [pred zs]
  (drop-while pred zs))

(función-drop-while-1 #(> 5 %) [1 2 3 4 5 6])
(función-drop-while-2 pos? [-1 -2 -6 -7 1 2 3 4 -5 -6 0 1])
(función-drop-while-3 neg? [-1 -6 -7 3 4 -5])

(defn función-every?-1
  [pred xs]
  (every? pred xs))

(defn función-every?-2
  [pred ys]
  (every? pred ys))

(defn función-every?-3
  [pred zs]
  (every? pred zs))

(función-every?-1 even? '(3 6 9))
(función-every?-2 {1 "one" 2 "two"} [1 2])
(función-every?-3 #{1 2} [1 2])

(defn función-filterv-1
  [pred xs]
  (filterv pred xs))

(defn función-filterv-2
  [pred ys]
  (filterv pred ys))

(defn función-filterv-3
  [pred zs]
  (filterv pred zs))

(función-filterv-1 even? (range 10))
(función-filterv-2 even? (range 5))
(función-filterv-3 #(= (count %) 1)
["a" "aa" "b" "n" "f" "lisp" "clojure" "q" ""])

(defn función-group-by-1
  [f xs]
  (group-by f xs))

(defn función-group-by-2
  [f ys]
  (group-by f ys))

(defn función-group-by-3
  [f zs]
  (group-by f zs))

(función-group-by-1 count ["h" "hi" "clojure" "ana" "hugo" "pala"])
(función-group-by-2 odd? (range 15))
(función-group-by-3 :user-id [{:user-id 2 :uri "/"}
                              {:user-id 3 :uri "/foo"}
                              {:user-id 1 :uri "/account"}])

(defn función-iterate-1
  [f x]
  (iterate f x))

(defn función-iterate-2
  [f x]
  (iterate f x))

(defn función-iterate-3
  [f x]
  (iterate f x))

(función-iterate-1 inc 5)
(función-iterate-2 dec 10)
(función-iterate-3 inc 2)

(defn función-keep-1
  [f xs]
  (keep f xs))

(defn función-keep-2
  [f ys]
  (keep f ys))

(defn función-keep-3
  [f zs]
  (keep f zs))

(función-keep-1 even? (range 10 15))
(función-keep-2 #(if (odd? %) %) (range 5))
(función-keep-3 seq [() [] '(1 2 3) [:a :b :c] nil])

(defn función-keep-indexed-1
  [f xs]
  (keep-indexed f xs))

(defn función-keep-indexed-2
  [f ys]
  (keep-indexed f ys))

(defn función-keep-indexed-3
  [f zs]
  (keep-indexed f zs))

(función-keep-indexed-1 #(if (odd? %1) %2) [:a :b :c :d :e])
(función-keep-indexed-2 #(if (pos? %2) %1) [-9 0 29 -7 45 3 -8])
(función-keep-indexed-3 (fn [idx v] (if (pos? v) idx)) [-9 0 29 -7 45 3 -8])

(defn función-map-indexed-1
  [f xs]
  (map-indexed f xs))

(defn función-map-indexed-2
  [f ys]
  (map-indexed f ys))

(defn función-map-indexed-3
  [f zs]
  (map-indexed f zs))

(función-map-indexed-1 hash-map "ejemplo")
(función-map-indexed-2 vector "clojure")
(función-map-indexed-3 list [:a :b :c :d])

(defn función-mapcat-1
  [f xs]
  (mapcat f xs))

(defn función-mapcat-2
  [f ys]
  (mapcat f ys))

(defn función-mapcat-3
  [f zs]
  (mapcat f zs))

(función-mapcat-1 reverse [[3 2 1 0] [6 5 4] [9 8 7]])
(función-mapcat-2 reverse [[6 2 5 4] [1 1 0 0] [5 8]])
(función-mapcat-3 reverse [[1 4 5 6] [3 6 1]])

(defn función-mapv-1
  [f xs]
  (mapv f xs))

(defn función-mapv-2
  [f xs ys]
  (mapv f xs ys))

(defn función-mapv-3
  [f xs ys]
  (mapv f xs ys))

(función-mapv-1 inc [1 2 3 4])
(función-mapv-2 + [4 5 6] [7 8 9])
(función-mapv-3 + [7 8 9] (iterate inc 2))

(defn función-merge-with-1
  [f & xs]
  (merge-with f xs))

(defn función-merge-with-2
  [f & ys]
  (merge-with f ys))

(defn función-merge-with-3
  [f & zs]
  (merge-with f zs))

(función-merge-with-1 into {"Lisp" ["Common Lisp" "Clojure"] "ML" ["Caml" "Objective Caml"]}
                      {"Lisp" ["Scheme"] "ML" ["Standard ML"]})
(función-merge-with-2 +
                      {:a 1  :b 2}
                      {:a 9  :b 98})
(función-merge-with-3 into
                      {:a #{1 2 3},   :b #{4 5 6}}
                      {:a #{2 3 7 8}, :c #{1 2 3}})

(defn función-not-any?-1
  [pred xs]
  (not-any? pred xs))

(defn función-not-any?-2
  [pred ys]
  (not-any? pred ys))

(defn función-not-any?-3
  [pred zs]
  (not-any? pred zs))

(función-not-any?-1 odd? '(5 10 15))
(función-not-any?-2 odd? '(1 2 3))
(función-not-any?-3 nil? [true false false])

(defn función-not-every?-1
  [pred xs]
  (not-every? pred xs))

(defn función-not-every?-2
  [pred ys]
  (not-every? pred ys))

(defn función-not-every?-3
  [pred zs]
  (not-every? pred zs))

(función-not-every?-1 odd? '(1 2 3 4 5))
(función-not-every?-2 odd? '(1 3))
(función-not-every?-3 odd? '(1 2 3 4))

(defn función-partition-by-1
  [f xs]
  (partition-by f xs))

(defn función-partition-by-2
  [f ys]
  (partition-by f ys))

(defn función-partition-by-3
  [f zs]
  (partition-by f zs))

(función-partition-by-1 #(= 4 %) [1 2 3 4 5])
(función-partition-by-2 odd? [1 1 1 2 2 3 3])
(función-partition-by-3 even? [1 1 1 2 2 3 3])

(defn función-reduce-kv-1
  [f init zs]
  (reduce-kv f init zs))

(defn función-reduce-kv-2
  [f init ys]
  (reduce-kv f init ys))

(defn función-reduce-kv-3
  [f init zs]
  (reduce-kv f init zs))

(función-reduce-kv-1 #(assoc %1 %2 %3) {} {:a 1 :b 2 :c 3})
(función-reduce-kv-2 #(assoc %1 %3 %4) {} {:a 1 :b 2 :c 3 :d 4})
(función-reduce-kv-3 #(assoc %1 %3 %2) {} {:a 1 :b 2 :c 3})

(defn función-remove-1
  [pred xs]
  (remove pred xs))

(defn función-remove-2
  [pred ys]
  (remove pred ys))

(defn función-remove-3
  [pred zs]
  (remove pred zs))

(función-remove-1 pos? [1 -2 2 -1 3 7 0 5 4])
(función-remove-2 nil? [1 nil 2 nil 3 nil])
(función-remove-3 even? (range 20))


(defn función-reverse-1
  [s]
  (reverse s))

(defn función-reverse-2
  [s]
  (reverse s))

(defn función-reverse-3
  [s]
  (reverse s))

(función-reverse-1 "clojure")
(función-reverse-2 "funcion")
(función-reverse-3 "visual")


(defn función-some-1
  [pred xs]
  (some pred xs))

(defn función-some-2
  [pred ys]
  (some pred ys))

(defn función-some-3
  [pred zs]
  (some pred zs))

(función-some-1 even? '(1 2 3 4))
(función-some-2 even? '(1 3 5 7))
(función-some-3 true? [false false false])

(defn función-sort-by-1
  [keyfn xs]
  (sort-by keyfn xs))

(defn función-sort-by-2
  [keyfn ys]
  (sort-by keyfn ys))

(defn función-sort-by-3
  [keyfn comp zs]
  (sort-by keyfn comp zs))

(función-sort-by-1 count ["hola" "studio" "count"])
(función-sort-by-2 first [[1 2] [2 2] [2 3]])
(función-sort-by-3 first > [[1 2] [2 2] [2 3]])


(defn función-split-with-1
  [pred xs]
  (split-with pred xs))

(defn función-split-with-2
  [pred ys]
  (split-with pred ys))

(defn función-split-with-3
  [pred zs]
  (split-with pred zs))

(función-split-with-1 (partial >= 4) [1 2 3 4 5])
(función-split-with-2 (partial > 5) [1 2 3 2 1 5 6])
(función-split-with-3 (partial > 11) [1 2 3 2 1])

(defn función-take-1
  [n xs]
  (take n xs))

(defn función-take-2
  [n ys]
  (take n ys))

(defn función-take-3
  [n zs]
  (take n zs))

(función-take-1 4 '(1 2 3 4 5 6))
(función-take-2 6 [1 2 3 4 5 6])
(función-take-3 3 [1 2])

(defn función-take-last-1
  [n xs]
  (take-last n xs))

(defn función-take-last-2
  [n ys]
  (take-last n ys))

(defn función-take-last-3
  [n zs]
  (take-last n zs))

(función-take-last-1 1 [1 2 3 4])
(función-take-last-2 2 [1 4])
(función-take-last-3 2 nil)

(defn función-take-nth-1
  [n xs]
  (take-nth n xs))

(defn función-take-nth-2
  [n ys]
  (take-nth n ys))

(defn función-take-nth-3
  [n zs]
  (take-nth n zs))

(función-take-nth-1 5 (range 10))
(función-take-nth-2 3 (range 20))
(función-take-nth-3 -10 (range 2))

(defn función-take-while-1
  [pred xs]
  (take-while pred xs))

(defn función-take-while-2
  [pred ys]
  (take-while pred ys))

(defn función-take-while-3
  [pred zs]
  (take-while pred zs))

(función-take-while-1 neg? [-2 -1 0 1 2 3])
(función-take-while-2 pos? [0 1 2 3])
(función-take-while-3 neg? [])

(defn función-update-1
  [m k f]
  (update m k f))

(defn función-update-2
  [m k f x]
  (update m k f x))

(defn función-update-3
  [m k f ]
  (update m k f ))

(función-update-1 {:name "Jusn" :age 29} :age dec)
(función-update-2 {:name "James" :age 26} :age + 10)
(función-update-3 {:name "Carlos" :age 26} :age dec)


(defn función-update-in-1
  [m ks f & args]
  (update-in m ks f args))

(defn función-update-in-2
  [m ks f & args]
  (update-in m ks f args))

(defn función-update-in-3
  [m ks f & args]
  (update-in m ks f args))

(función-update-in-1 {:name "James" :age 26} [:age] inc)
(función-update-in-2 {:name "carlos" :age 25} [:age] + 10)
(función-update-in-3 {:name "ana" :age 30} [:age] - 10)








